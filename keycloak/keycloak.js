// @flow
import passport from "@outlinewiki/koa-passport";
import Router from "koa-router";
// import { Strategy as SlackStrategy } from "passport-slack-oauth2";
import { Strategy as KeycloakStrategy } from "passport-keycloak-oauth2-oidc";
import accountProvisioner from "../../commands/accountProvisioner";
import env from "../../env";
import auth from "../../middlewares/authentication";
import passportMiddleware from "../../middlewares/passport";
import {
  IntegrationAuthentication,
  Collection,
  Integration,
  Team,
} from "../../models";
// import * as Slack from "../../slack";
import { StateStore } from "../../utils/passport";

const router = new Router();
const providerName = "keycloak";
const KEYCLOAK_CLIENT_ID = process.env.KEYCLOAK_KEY; // needs to be defined
const KEYCLOAK_CLIENT_SECRET = process.env.KEYCLOAK_SECRET; // needs to be defined
const KEYCLOAK_REALM = process.env.KEYCLOAK_REALM; // needs to be defined

export const config = {
  name: "Keycloak",
  enabled: !!KEYCLOAK_CLIENT_ID,
};

if(KEYCLOAK_CLIENT_ID){
    const strategy = new KeycloakStrategy(
        {
            clientID:KEYCLOAK_CLIENT_ID,
            realm:KEYCLOAK_REALM,
            publicClient:'false',
            clientSecret:KEYCLOAK_CLIENT_SECRET,
            sslRequired:'external',
            authServerURL: 'https://keycloak.example.com/auth',
            callbackURL: `${env.URL}/keycloak/callback`
        },
        function(accessToken, refreshToken, profile, done) {
            try {
                const result = await accountProvisioner({
                  ip: env.URL,//req.ip ,
                  team: {
                    name: profile.team.name,
                    subdomain: profile.team.domain,
                    avatarUrl: profile.team.image_230,
                  },
                  user: {
                    name: profile.user.name,
                    email: profile.user.email,
                    avatarUrl: profile.user.image_192,
                  },
                  authenticationProvider: {
                    name: providerName,
                    providerId: profile.team.id,
                  },
                  authentication: {
                    providerId: profile.user.id,
                    accessToken,
                    refreshToken,
                    scopes,
                  },
                });
                console.log({result});
                return done(null, result.user, result);
              } catch (err) {
                return done(err, null);
              }
        });

    // For some reason the author made the strategy name capatilised, I don't know
    // why but we need everything lowercase so we just monkey-patch it here.
    strategy.name = providerName;
    passport.use(strategy);

    router.get('/auth/keycloak',  passport.authenticate('keycloak', { scope: ['profile'] }));

    router.get('/auth/keycloak/callback', passport.authenticate('keycloak', { failureRedirect: '/login' }),
        function(req, res) {
          // Successful authentication, redirect home.
          res.redirect('/');
        });
}

export default router;
