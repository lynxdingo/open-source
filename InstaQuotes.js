'use strict';

const fs = require('fs');

const { SVG } = require('@svgdotjs/svg.js');

const dir = "./assets/svg/";
const DIMENSION = 1080; // size of the SVG file

/**
 * @class InstaQuotes
 * @description Takes data from the `inputFile` file and writes it to the `outputFile` file.
 * @param {string} version 1 or 2
 * @param {FILE} inputFile TXT or JSON
 * @param {FILE} outputFile JSON or SVG
 * @version 1.0 can read from TXT and write to JSON
 * @version 2.0 can read from JSON and write to SVG
 * @author lynxdingo
 */
module.exports = class InstaQuotes {
    constructor(version, inputFile, outputFile) {
        switch (version) {
            case '1':
                this.version1(inputFile, outputFile);

                this.json = []; // holds the JSON data

                break;
            case '2':
                this.version2(inputFile, outputFile);
                break;
            default:
                break;
        }
    }

    /**
     * Takes data from a TXT file and writes it to a JSON file.
     * @class InstaQuotes-v1
     * @memberof InstaQuotes
     * @param {TXT} inputFile
     * @param {JSON} outputFile
     */
    version1(inputFile, outputFile) {
        this.inputFile = (!inputFile) ? "seneca.txt" : inputFile;
        this.seneca = "seneca.json";
        this.quotes = "quotes.json";
        this.insta = "instaQuotes.json";
        this.outputFile = (!outputFile) ? "instaQuotes.json" : outputFile;
    }

    /**
     * Takes data from a JSON file and writes it to a SVG file.
     * @class InstaQuotes-v2
     * @memberof InstaQuotes
     * @param {JSON} inputFile
     * @param {SVG} outputFile
     */
    version2(inputFile, outputFile) {
        this.inputFile = (!inputFile) ? "instaQuotes.json" : inputFile;
        this.outputFile = (!outputFile) ? "test.svg" : outputFile;
    }

    /**
     * Creates an SVG file.
     * @memberof InstaQuotes-v2
     * @param {number} index quote number prepended before the `title`
     * @param {string} title
     * @param {string} quote
     * @param {number} quoteIndex the quote number, used for saving purposes
     * @see {@link https://github.com/svgdotjs/svgdom}
     * @see {@link https://svgjs.com/docs/2.7/elements/#svg-text}
     */
    createSVG(index, title, quote, quoteIndex) {
        const LIGHT_BLUE = '#aaeeff';

        // returns a window with a document and an svg root node
        const window = require('svgdom')
        const document = window.document
        const { SVG, registerWindow } = require('@svgdotjs/svg.js')

        // register window and document
        registerWindow(window, document)

        // create canvas
        const canvas = SVG(document.documentElement).size(DIMENSION, DIMENSION)

        // use svg.js as normal
        canvas.rect(DIMENSION, DIMENSION).fill('black')

        // CONSTS
        const CHAR_TEXT_SIZE = 20;
        const CHAR_TITLE_SIZE = 40;

        // CALCULATIONS
        const textSegments = this.createTextSegments(quote, 50, CHAR_TEXT_SIZE);
        const titleSegments = this.createTextSegments(title.toUpperCase(), 24, CHAR_TITLE_SIZE);

        const placeDeltaY = this.calcPosY(titleSegments, textSegments);

        // TITLE NUMBER
        canvas.text(add => {
            add.tspan(`${index}.`);
        }).font({ family: 'sans-serif', fill: LIGHT_BLUE, size: '40pt', weight: 'bold' }).move(20, placeDeltaY + 70);
        // TITLE TEXT
        canvas.text(add => {
            titleSegments.forEach(s => {
                add.tspan(s.text).newLine()
            });
        }).font({ family: 'sans-serif', fill: LIGHT_BLUE, size: '40pt', weight: 'bold' }).move(135, placeDeltaY + 18);

        // QUOTE
        canvas.text(add => {
            textSegments.forEach(s => {
                add.tspan(s.text).newLine().dx(s.dX)
            });
        }).font({ family: 'sans-serif', fill: LIGHT_BLUE, size: '28pt' }).move(30, placeDeltaY + titleSegments.length * 80 + 16);

        // AUTHOR
        const authorPlaceDeltaY = placeDeltaY + titleSegments.length * CHAR_TITLE_SIZE + textSegments.length * CHAR_TEXT_SIZE;

        canvas.text(this.getAuthor(quote)).move(DIMENSION - 250, authorPlaceDeltaY + 240).font({ fill: LIGHT_BLUE, size: '20pt', family: 'sans-serif' });

        // CONSTANTS
        // INSTAGRAM
        canvas.text(add => {
            add.tspan('@barefootstache').newLine();
            add.tspan('').newLine();
            add.tspan('#bfsquote').newLine();
        }).move(DIMENSION - 250, DIMENSION - 130).font({ fill: 'white', size: '20pt', family: 'sans-serif' });

        // LOGOS
        canvas.svg(fs.readFileSync(dir + 'instaLogos.svg', 'utf8')); // holds the logos

        // CREATE FILE
        quoteIndex++;
        if (quoteIndex < 10) {
            quoteIndex = `0${quoteIndex}`;
        }

        const svgFileName = `${index}-${quoteIndex}`;

        fs.writeFile(`${dir}quotes/${svgFileName}.svg`, canvas.svg(), err => {
            if (err) {
                throw err;
            }
        });
    }

    /**
     * Creates an array of text segments with the starting value of the X-position.
     * @memberof InstaQuotes-v2
     * @param {string} quote
     * @param {number} MAX_LENGTH the maximum number of charachters in respect to `CHAR_SIZE` within a line
     * @param {number} CHAR_SIZE the charachter size
     * @returns {any[]} with element as {text:string,dX:number}
     */
    createTextSegments(quote, MAX_LENGTH, CHAR_SIZE) {
        const words = quote.split(' ');
        const segments = [];
        // const MAX_LENGTH = 60;
        // const CHAR_SIZE = 18;

        // only for quotes
        if (CHAR_SIZE === 18) {
            // first word missing quotation symbol
            if (!words[0].includes('"')) {
                words[0] = `"${words[0]}`;
            }
            // last word missing quotation symbol
            if (!words[words.length - 1].includes('"')) {
                words[words.length - 1] += '"';
            }
        }

        let noMoreWords = false;

        for (let i = 0, j = 0; words[i] && !noMoreWords; j++) {
            let text = '';
            while (words[i] && text.length <= MAX_LENGTH - words[i].length) {
                if (!words[i]) {
                    noMoreWords = true;
                    break;
                }

                text += `${words[i]} `;
                i++;
            }
            segments[j] = {
                text: text,
                dX: Math.floor((DIMENSION - text.length * CHAR_SIZE) / 2)
            };
        }
        return segments;
    }

    /**
     * Calculates the Y-position, where the text segment should be placed within the SVG file.
     * @memberof InstaQuotes-v2
     * @param {string[]} titleSegs
     * @param {string[]} quoteSegs
     * @returns {number}
     */
    calcPosY(titleSegs, quoteSegs) {
        const TITLE_HEIGHT = 54;
        const DELTA = 16;
        const TEXT_HEIGHT = 38;
        const LOGOS_HEIGHT = 200;

        const totalInnerHeight = titleSegs.length * TITLE_HEIGHT + quoteSegs.length * TEXT_HEIGHT + 2 * DELTA;

        return Math.floor((DIMENSION - totalInnerHeight - LOGOS_HEIGHT) / 2);
    }

    /**
     * Gets the author. If the `quote` has a `"` charachter then _Seneca_ else _Franjo Lukežić_.
     * @memberof InstaQuotes-v2
     * @param {string} quote
     * @returns {string} author
     */
    getAuthor(quote) {
        if (quote.includes('\"')) {
            return '- Seneca';
        } else {
            return '- Franjo Lukežić';
        }
    }

    /**
     * @memberof InstaQuotes-v1
     * @param {any[]} arr
     */
    addObjectToJson(arr) {
        arr.forEach((item, index) => {
            if (item.length != 0) {
                let tObject = {
                    index: index,
                    quotes: item.split('\r\n')
                };

                this.json.push(tObject);
            }
        });
    }

    /**
     *  @memberof InstaQuotes-v1
     * @returns {any[]}
     */
    createItemsByNewLine() {
        return new Promise((resolve, reject) => {
            fs.readFile(this.seneca, 'utf8', (err, sen) => {
                if (err) throw err;
                sen = JSON.parse(sen);
                fs.readFile(this.quotes, 'utf8', (err, qut) => {
                    if (err) throw err;
                    qut = JSON.parse(qut);

                    for (let i = 0; i < sen.length && i < qut.length; i++) {
                        let tObject = {
                            index: -1,
                            title: '',
                            quotes: []
                        };

                        if (sen[i].index === qut[i].index) {
                            tObject.index = sen[i].index;
                            tObject.title = sen[i].title;
                            tObject.quotes = qut[i].quotes;

                            this.json.push(tObject);
                        }
                    }
                    resolve(this.json);
                });
            });
        });
    }

    /**
     *
     * @memberof InstaQuotes-v1
     */
    run() {
        this.createItemsByNewLine().then(arr => {
            // this.addObjectToJson(arr);
            fs.writeFile(this.outputFile, JSON.stringify(this.json));
            console.log('complete');
        });
    }
}