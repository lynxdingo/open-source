'use strict';
const fs = require('fs');

/**
 * Simple read and write functions for JSON and text files.
 * @author lynxdingo
 * @license MIT
 */
module.exports = class InputOutput {
    /**
     * Reads a file, splits it, and returns the splitted result
     * @param {path} inputFile path to input file, @default input.txt
     * @param {string} splitChars pattern to split by
     * @return {Promise<any[]>} splitted array
     */
    static readSplitBy(inputFile, splitChars) {
        if (!inputFile) {
            inputFile = './assets/input.txt';
        } else {
            inputFile = './assets/txt/' + inputFile + '.txt'
        }
        if (!splitChars) {
            splitChars = '\r\n';
        }
        console.log({ in: inputFile, split: splitChars });

        return new Promise((resolve, reject) => {
            fs.readFile(inputFile, 'utf8', (err, data) => {
                if (err) throw err;
                try {
                    let result = data.split(splitChars);
                    resolve(result);
                } catch (error) {
                    console.error(error);
                    reject([]);
                }
            });
        });
    };

    /**
     * Reads a json file and returns the JSON parsed version
     * @param {path} inputFile path to input file, @default input.json
     * @returns {Promise<JSON>}
     */
    static readJson(inputFile) {
        if (!inputFile) {
            inputFile = './assets/input.json';
        } else {
            inputFile = './assets/json/' + inputFile + '.json'
        }
        console.log({ in: inputFile });

        return new Promise((resolve, reject) => {
            fs.readFile(inputFile, 'utf8', (err, jsonData) => {
                if (err) throw err;
                try {
                    let result = JSON.parse(jsonData);
                    resolve(result);
                } catch (error) {
                    console.error(error);
                    reject({});
                }
            });
        });
    };

    /**
     * Writes to an output file
     * @param {path} outputFile path to output file @default output.txt
     * @param {any | string} data data to be written
     * @returns {Promise<boolean>}
     */
    static writeFile(outputFile, data) {
        if (!outputFile) {
            outputFile = './assets/output.txt';
        } else {
            outputFile = './assets/txt/' + outputFile + '.txt'
        }
        console.log({ out: outputFile });

        return new Promise((resolve, reject) => {
            fs.writeFile(outputFile, data, (err) => {
                if (err) throw err;
                console.log('%s\x1b[36m%s\x1b[0m\x1b[32m%s\x1b[0m',
                    'Wrote to file: ',
                    outputFile,
                    ' - COMPLETED', );
                resolve(true);
            });
        });
    };

    /**
     * Writes to a json file.
     * @param {path} outputFile path to output file @default output.json
     * @param {JSON} dataJson
     * @returns {Promise<boolean>}
     */
    static writeJsonFile(outputFile, dataJson) {
        if (!outputFile) {
            outputFile = './assets/output.json';
        } else {
            outputFile = './assets/json/' + outputFile + '.json'
        }
        if (!dataJson) {
            throw 'No JSON data to be written';
        }
        console.log({ out: outputFile, data: (dataJson == true) });

        return new Promise((resolve, reject) => {
            fs.writeFile(outputFile, JSON.stringify(dataJson), (err) => {
                if (err) throw err;
                console.log('%s\x1b[36m%s\x1b[0m\x1b[32m%s\x1b[0m',
                    'Wrote to file: ',
                    outputFile,
                    ' - COMPLETED', );
                resolve(true);
            });
        });
    };
}