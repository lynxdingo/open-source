## open-source

A list of scripts for personal use.

## Snippets

These snippets are being used at various spots on the [Barefoot Stache](https://www.barefootstache.com/) website.

### Components

A list of HTML components.

- [Icon Header](https://gitlab.com/lynxdingo/open-source/snippets/1861521)

### Scripts

A list of JavaScript functions.

- [Instagram Quotes](InstaQuotes.js) ([How To](#instragram-quotes))
- [Newly added list items](https://gitlab.com/lynxdingo/open-source/snippets/1861531)

### Styles

A list of styles.

- [main.css](https://gitlab.com/lynxdingo/open-source/snippets/1861517)
- [additional.css](https://gitlab.com/lynxdingo/open-source/snippets/1861534)
- [tabs.css](https://gitlab.com/lynxdingo/open-source/snippets/1914073)

## How To
### Instagram Quotes

Choose your version, 1 or 2 
```
const insta = new InstaQuotes(version)
```

For `version = 2` run `createSVG` as
```
insta.createSVG(titleIndex, title, quote, quoteIndex)
```

with 
```
titleIndex:number, // prepended to the title
title:string,
quote:string,
quoteIndex:number // used for saving purposes
```

_**Note** version 1 is not sturdy, use on own risk_


## License
[MIT License](https://gitlab.com/lynxdingo/open-source/blob/master/LICENSE)